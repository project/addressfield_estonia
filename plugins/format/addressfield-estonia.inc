<?php

/**
 * @file
 * A specific handler for CH.
 */

$plugin = array(
  'title' => t('Addressfield optimized for Estonia'),
  'format callback' => 'addressfield_format_address_et_generate',
  'type' => 'address',
  'weight' => -80,
);

function addressfield_format_address_et_generate(&$format, $address, $context = array()) {
  if ($address['country'] == 'EE') {
    unset($format['street_block']['premise']);
    $format['street_block']['#attributes']['class'][] = 'addressfield-container-inline';
    $format['street_block']['thoroughfare'] = array(
      '#title' => t('Address'),
      '#size' => 30,
      '#required' => TRUE,
    );
    $places = _addressfield_estonia_places();
    $counties = array_keys($places);
    $format['locality_block']['administrative_area'] = array(
      '#title' =>  t('County'),
      '#required' => TRUE,
      '#prefix' => ' ',
      '#render_option_value' => TRUE,
      '#attributes' => array(
        'class' => array('administrative_area'),
        'x-autocompletetype' => 'administrative_area',
        'autocomplete' => 'administrative_area',
      ),
      '#weight' => -10,
      '#empty_value' => '',
      '#options' => array_combine($counties, $counties),
    );
    $format['street_block']['#weight'] = 80;
    $format['street_block']['postal_code'] = $format['locality_block']['postal_code'];
    unset($format['locality_block']['postal_code']);
    $format['street_block']['postal_code']['#weight'] = 100;

    $locality = $address['administrative_area'] ? $places[$address['administrative_area']] : array();
    $format['locality_block']['locality'] = array(
      '#title' => t('City/Parish'),
      '#type' => 'select',
      '#options' => array_combine($locality, $locality),
      '#empty_value' => '',
      '#required' => TRUE,
      '#render_option_value' => TRUE,
    );
    if ($context['mode'] == 'render') {
      $format['street_block']['postal_code']['#prefix'] = '<br />';
      $format['locality_block']['locality']['#suffix'] = '<br />';
      $format['locality_block']['administrative_area']['#suffix'] = ', ';
    }
    if ($context['mode'] == 'form') {
      if (!isset($format['#wrapper_id'])) {
        $format['#wrapper_id'] = drupal_html_id('addressfield-wrapper');
        $format['#prefix'] = '<div id="' . $format['#wrapper_id'] . '">';
        $format['#suffix'] = '</div>';
      }

      // AJAX enable it.
    $format['locality_block']['administrative_area']['#ajax'] = array(
      'callback' => 'addressfield_standard_widget_refresh',
      'wrapper' => $format['#wrapper_id'],
    );
    }
  }
}

function _addressfield_estonia_places() {
  $list = array(
    'Harju maakond' => array(
      'Keila linn',
      'Loksa linn',
      'Maardu linn',
      'Paldiski linn',
      'Saue linn',
      'Tallinn',
      'Aegviidu vald',
      'Anija vald',
      'Harku vald',
      'Jõelähtme vald',
      'Keila vald',
      'Kernu vald',
      'Kiili vald',
      'Kose vald',
      'Kuusalu vald',
      'Nissi vald',
      'Padise vald' ,
      'Raasiku vald',
      'Rae vald',
      'Saku vald',
      'Saue vald',
      'Vasalemma vald',
      'Viimsi vald',
    ),
    'Hiiu maakond' => array(
      'Emmaste vald',
      'Hiiu vald',
      'Käina vald',
      'Pühalepa vald',
    ),
    'Ida-Viru maakond' => array(
      'Kiviõli linn',
      'Kohtla-Järve linn',
      'Narva linn',
      'Narva-Jõesuu linn',
      'Sillamäe linn',
      'Alajõe vald',
      'Aseri vald',
      'Avinurme vald',
      'Iisaku vald',
      'Illuka vald',
      'Jõhvi vald',
      'Kohtla vald',
      'Kohtla-Nõmme vald',
      'Lohusuu vald',
      'Lüganuse vald',
      'Mäetaguse vald',
      'Sonda vald',
      'Toila vald',
      'Tudulinna vald',
      'Vaivara vald',
    ),
    'Jõgeva maakond' => array(
      'Jõgeva linn',
      'Mustvee linn',
      'Põltsamaa linn',
      'Jõgeva vald',
      'Pajusi vald',
      'Pala vald',
      'Palamuse vald',
      'Puurmani vald',
      'Põltsamaa vald',
      'Kasepää vald',
      'Saare vald',
      'Tabivere vald',
      'Torma vald',
    ),
    'Järva maakond' => array(
      'Paide linn',
      'Albu vald',
      'Ambla vald',
      'Imavere vald',
      'Järva-Jaani vald',
      'Kareda vald',
      'Koeru vald',
      'Koigi vald',
      'Paide vald',
      'Roosna-Alliku vald',
      'Türi vald',
      'Väätsa vald',
    ),
    'Lääne maakond' => array(
      'Haapsalu linn',
      'Hanila vald',
      'Kullamaa vald',
      'Lihula vald',
      'Lääne-Nigula vald',
      'Martna vald',
      'Noarootsi vald',
      'Nõva vald',
      'Ridala vald',
      'Vormsi vald',
    ),
    'Lääne-Viru maakond' => array(
      'Kunda linn',
      'Rakvere linn',
      'Haljala vald',
      'Kadrina vald',
      'Laekvere vald',
      'Rakke vald',
      'Rakvere vald',
      'Rägavere vald',
      'Sõmeru vald',
      'Tamsalu vald',
      'Tapa vald',
      'Vihula vald',
      'Vinni vald',
      'Viru-Nigula vald',
      'Väike-Maarja vald',
    ),
    'Põlva maakond' => array(
      'Ahja vald',
      'Kanepi vald',
      'Kõlleste vald',
      'Laheda vald',
      'Mikitamäe vald',
      'Mooste vald',
      'Orava vald',
      'Põlva vald',
      'Räpina vald',
      'Valgjärve vald',
      'Vastse-Kuuste vald',
      'Veriora vald',
      'Värska vald',
    ),
    'Pärnu maakond' => array(
      'Pärnu linn',
      'Sindi linn',
      'Are vald',
      'Audru vald',
      'Halinga vald',
      'Häädemeeste vald',
      'Kihnu vald',
      'Koonga vald',
      'Paikuse vald',
      'Saarde vald',
      'Sauga vald',
      'Surju vald',
      'Tootsi vald',
      'Tori vald',
      'Tõstamaa vald',
      'Tahkuranna vald',
      'Varbla vald',
      'Vändra vald',
      'Vändra vald (alev)',
    ),
    'Rapla maakond' => array(
      'Juuru vald',
      'Järvakandi vald',
      'Kaiu vald',
      'Kehtna vald',
      'Kohila vald',
      'Käru vald',
      'Märjamaa vald',
      'Raikküla vald',
      'Rapla vald',
      'Vigala vald',
    ),
    'Saare maakond' => array(
      'Kuressaare linn',
      'Kaarma vald',
      'Kihelkonna vald',
      'Kärla vald',
      'Laimjala vald',
      'Leisi vald',
      'Lümanda vald',
      'Muhu vald',
      'Mustjala vald',
      'Orissaare vald',
      'Pihtla vald',
      'Pöide vald',
      'Ruhnu vald',
      'Salme vald',
      'Torgu vald',
      'Valjala vald',
    ),
    'Tartu maakond' => array(
      'Elva linn',
      'Kallaste linn',
      'Tartu linn',
      'Alatskivi vald',
      'Haaslava vald',
      'Kambja vald',
      'Konguta vald',
      'Laeva vald',
      'Luunja vald',
      'Meeksi vald',
      'Mäksa vald',
      'Nõo vald',
      'Peipsiääre vald',
      'Piirissaare vald',
      'Puhja vald',
      'Rannu vald',
      'Rõngu vald',
      'Tartu vald',
      'Tähtvere vald',
      'Vara vald',
      'Võnnu vald',
      'Ülenurme vald',
    ),
    'Valga maakond' => array(
      'Tõrva linn',
      'Valga linn',
      'Helme vald',
      'Hummuli vald',
      'Karula vald',
      'Palupera vald',
      'Puka vald',
      'Põdrala vald',
      'Otepää vald',
      'Sangaste vald',
      'Taheva vald',
      'Tõlliste vald',
      'Õru vald',
    ),
    'Viljandi maakond' => array(
      'Mõisaküla linn',
      'Viljandi linn',
      'Võhma linn',
      'Abja vald',
      'Halliste vald',
      'Kolga-Jaani vald',
      'Kõo vald',
      'Kõpu vald',
      'Karksi vald',
      'Suure-Jaani vald',
      'Tarvastu vald',
      'Viljandi vald',
    ),
    'Võru maakond' => array(
      'Võru linn',
      'Antsla vald',
      'Haanja vald',
      'Lasva vald',
      'Meremäe vald',
      'Misso vald',
      'Mõniste vald',
      'Rõuge vald',
      'Sõmerpalu vald',
      'Urvaste vald',
      'Varstu vald',
      'Vastseliina vald',
      'Võru vald',
    ),
  );
  return $list;
}
